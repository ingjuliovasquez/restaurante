<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RestaurantesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $form = json_decode($this->form, true);

        $this->merge([
            'nombre' =>$form['nombre'],
            'descripcion' =>$form['descripcion'],
            'telefono' =>$form['telefono'],
            'contacto' =>$form['contacto'],
            'correo_electronico' =>$form['correo_electronico'],
            'rfc' =>$form['rfc'],
            'direccion' =>$form['direccion'],
            'imagen_logo' => $this->imagen_logo,
            'imagen_fondo_aplicacion_movil' => $this->imagen_fondo_aplicacion_movil
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'nombre' => [
                'required',
                'max:50',
                Rule::unique('restaurantes')->ignore($this->restaurante),
            ],
            'descripcion' => 'required|max:150',
            'telefono' => 'required|max:15',
            'contacto' => 'required|max:30',
            'rfc' => 'max:13',
            'correo_electronico' => 'required|regex:/(.+)@(.+)\.(.+)/i',
            'direccion' => 'required|max:50',
            'imagen_logo' => 'required|mimes:png,jpg,jpeg',
            'imagen_fondo_aplicacion_movil' => 'required|mimes:png,jpg,jpeg',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Por favor ingrese un nombre de restaurante',
            'nombre.unique' => 'Este nombre de restaurante ya esta en uso',
            'descripcion.required' => 'Por favor ingrese, la descripción del restaurante',
            'descripcion.max' => 'Por favor, ingrese un valor, entre 1 y 50 digitos',
            'telefono.required' => 'Por favor ingrese, el teléfono del restaurante',
            'telefono.max' => 'Por favor, ingrese un valor, entre 1 y 15 digitos',
            'contacto.required' => 'Por favor ingrese, el contacto del restaurante',
            'contacto.max' => 'Por favor, ingrese un valor, entre 1 y 30 digitos',
            'correo_electronico.required' => 'Por favor ingrese, el correo del restaurante',
            'correo_electronico.max' => 'Por favor, ingrese un valor, entre 1 y 50 digitos',
            'correo_electronico.email' => 'Por favor ingrese un email valido',
            'direccion.required' => 'Por favor ingrese, la dirección del restaurante',
            'direccion.max' => 'Por favor, ingrese un valor, entre 1 y 150 digitos',
            'rfc.max' => 'Por favor, ingrese un valor, entre 1 y 13 digitos',
            'imagen_logo.required' => 'Por favor, seleccione el logo del restaurante',
            'imagen_logo.mimes' => 'La imagen debe tener formato png, jpg o jpeg',
            'imagen_fondo_aplicacion_movil.required' => 'Por favor, seleccione el fondo de la app movil',
            'imagen_fondo_aplicacion_movil.mimes' => 'La imagen debe tener formato png, jpg o jpeg',
        ];
    }
}
