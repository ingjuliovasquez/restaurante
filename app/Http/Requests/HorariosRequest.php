<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HorariosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            '*.hora_i' => ['required'],
            '*.hora_f' => ['required'],
        ];
    }

    public function messages()
    {
        return [
            '*.hora_i.required' => 'Por favor, ingrese una hora inicial',
            '*.hora_f.required' => 'Por favor, ingrese una hora final',
        ];
    }
}
