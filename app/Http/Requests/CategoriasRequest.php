<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CategoriasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $form = json_decode($this->form, true);

        $this->merge([
            'nombre' =>$form['nombre'],
            'prioridad' =>$form['prioridad'],
            'imagen' => $this->imagen
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => [
                'required',
                'min:4',
                'max:50',
                Rule::unique('categorias')->ignore($this->categoria),
            ],
            'prioridad' => 'required|max:2',
            'imagen' => 'mimes:png,jpg,jpeg|nullable',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Por favor ingrese un nombre de categoría',
            'nombre.unique' => 'Este nombre de categoría ya esta en uso',
            'prioridad.required' => 'Por favor ingrese, la prioridad de la categoría',
            'prioridad.max' => 'Por favor, ingrese un valor, entre 1 y 2 dígitos',
            'nombre.max' => 'Por favor, ingrese un valor, entre 4 y 50 dígitos',
            'nombre.min' => 'Por favor, ingrese un valor, entre 4 y 50 dígitos',
            'imagen.mimes' => 'La imagen debe tener formato png, jpg o jpeg',
        ];
    }
}
