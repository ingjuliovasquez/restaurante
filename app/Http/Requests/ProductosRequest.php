<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $form = json_decode($this->form, true);

        $this->merge([
            'nombre' =>$form['nombre'],
            'descripcion' =>$form['descripcion'],
            'preparacion' =>$form['preparacion'],
            'id_categoria' =>$form['id_categoria'],
            'costo' =>$form['costo'],
            'imagen' => $this->imagen
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|max:50|min:4',
            'descripcion' => 'required|max:150|min:1',
            'preparacion' => 'required|max:255|min:1',
            'costo' => 'required|max:12',
            'id_categoria' => 'required',
            'imagen' => 'mimes:png,jpg,jpeg|nullable',
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'Por favor ingrese un nombre de un producto',
            'nombre.min' => 'Por favor, ingrese un valor, entre 1 y 50 dígitos',
            'nombre.max' => 'Por favor, ingrese un valor, entre 1 y 50 dígitos',
            'descripcion.required' => 'Por favor ingrese, la descripción del producto',
            'descripcion.max' => 'Por favor, ingrese un valor, entre 1 y 150 dígitos',
            'descripcion.min' => 'Por favor, ingrese un valor, entre 1 y 150 dígitos',
            'preparacion.required' => 'Por favor ingrese, la preparación del producto',
            'preparacion.max' => 'Por favor, ingrese un valor, entre 1 y 255 dígitos',
            'preparacion.min' => 'Por favor, ingrese un valor, entre 1 y 255 dígitos',
            'costo.required' => 'Por favor ingrese, el costo del producto',
            'costo.max' => 'Por favor, ingrese un valor, entre 1 y 12 dígitos',
            'id_categoria.required' => 'Por favor, seleccione una categoría',
            'imagen.mimes' => 'La imagen debe tener formato png, jpg o jpeg',
        ];
    }
}
