<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ZonasEntregasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $form = $this->form;
        $coors = $this->coors;

        $this->merge([
            'nombre' =>$form['nombre'],
            'costo_envio' =>$form['costo_envio'],
            'compra_minima' =>$form['compra_minima'],
            'tiempo_traslado' =>$form['tiempo_traslado'],
            'zona_perimetral' => $coors
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|max:50',
            'costo_envio' => 'required|max:12',
            'compra_minima' => 'required|max:13',
            'tiempo_traslado' => 'required|max:2',
            'zona_perimetral' => 'required',
        ];
    }


    public function messages()
    {
        return [
            'nombre.required' => 'Por favor ingrese un nombre de zona de entrega',
            'nombre.max' => 'Por favor, ingrese un valor, entre 1 y 50 digitos',
            'costo_envio.required' => 'Por favor ingrese un costo de envio',
            'costo_envio.max' => 'Por favor, ingrese un valor, entre 1 y 12 digitos',
            'compra_minima.required' => 'Por favor ingrese una  compra minima',
            'compra_minima.max' => 'Por favor, ingrese un valor, entre 1 y 13 digitos',
            'tiempo_traslado.required' => 'Por favor ingrese un tiempo de traslado',
            'tiempo_traslado.max' => 'Por favor, ingrese un valor, entre 1 y 3 digitos',
            'zona_perimetral.required' => 'Por favor, agregue una zona/linea perimetral',
        ];
    }
}
