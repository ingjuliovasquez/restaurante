<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UsuariosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $form = json_decode($this->form, true);

        $this->merge([
            'name' => $form['name'],
            'email' => $form['email'],
            'password' => $form['password']
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($this->usuario),
            ],
            'name' => 'required|max:30',
            'password' => 'required|min:6max:128',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Por favor ingrese un nombre',
            'name.max' => 'Por favor, ingrese un valor, entre 1 y 30 digitos',
            'email.unique' => 'Este correo ya esta en uso',
            'email.required' => 'por favor ingrese un email valido',
            'email.email' => 'Por favor ingrese un email valido',
            'password.required' => 'Por favor ingrese una contraseña',
            'password.max' => 'Por favor, ingrese un contraseña, entre 6 y 128 digitos',
            'password.min' => 'Por favor, ingrese un contraseña, entre 6 y 128 digitos',
        ];
    }
}