<?php

namespace App\Http\Controllers;

use App\Models\FormasPagos;
use App\Models\FormasPagosRestaurantes;
use App\Models\Restaurantes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Inertia\Inertia;

class FormasPagosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restaurantes = Restaurantes::all()->map(function ($restaurante) {
            return [
                'id' => $restaurante->id,
                'nombre' => $restaurante->nombre,
                'formasPago' => $restaurante->formasPago
            ];
        });

        $formasPago = FormasPagos::all()->map(function ($forma) {
            return [
                'id' => $forma->id,
                'nombre' => $forma->nombre,
                'descripcion' => $forma->descripcion,
                'activo' => $forma->activo,
                'check' => false
            ];
        });

        return Inertia::render('Formas_Pago/Index', [
            'restaurantes' => $restaurantes,
            'formasPago' => $formasPago
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        foreach ($request->all() as $formaPago) {
            if ($formaPago['check']) {
                Validator::make($formaPago, [
                    'id_restaurante' => 'required',
                    'id' => 'required',
                ])->validate();
                // dd($formaPago);
                FormasPagosRestaurantes::updateOrCreate(
                    [
                        'id_forma_pago' => $formaPago['id'],
                        'id_restaurante' => $formaPago['id_restaurante'],
                    ],
                    [
                        'activo' => true,
                    ]
                );
            } else {
                FormasPagosRestaurantes::where([
                    ['id_forma_pago', $formaPago['id']],
                    ['id_restaurante', $formaPago['id_restaurante']]
                ])->delete();
            }
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FormasPagos  $formasPagos
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     return 'hello world';
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FormasPagos  $formasPagos
     * @return \Illuminate\Http\Response
     */
    public function edit(FormasPagos $formasPagos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FormasPagos  $formasPagos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FormasPagos $formasPagos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FormasPagos  $formasPagos
     * @return \Illuminate\Http\Response
     */
    public function destroy(FormasPagos $formasPagos)
    {
        //
    }

    public function pagosActivo($id)
    {
        $forma = FormasPagos::find($id);
        $forma->activo = $forma->activo == 0 ? 1 : 0;
        $forma->save();
        return back();
    }
}