<?php

namespace App\Http\Controllers;

use App\Models\Categorias;
use App\Models\Restaurantes;
use Illuminate\Http\Request;
use Inertia\Inertia;
use App\Http\Requests\CategoriasRequest;
use Illuminate\Support\Facades\Storage;

class CategoriasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Categorias/Index', [
            'restaurantes' => Restaurantes::all()->map(function ($restaurante) {
                return [
                    'id' => $restaurante->id,
                    'nombre' => $restaurante->nombre,
                    'categorias' => $restaurante->categorias
                ];
            })
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function formulario($id)
    {
        $restaurante = Restaurantes::find($id, ['id', 'nombre']);
        return Inertia::render('Categorias/Form', ['restaurante' => $restaurante]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoriasRequest $request)
    {
        $form = json_decode($request->form, true);

        if ($request->hasFile('imagen')) {
            $url_imagen = Storage::putFile('public/categorias', $request['imagen']);
            $form['imagen'] = $url_imagen;
        }

        Categorias::create($form);
        return redirect("categorias");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function show(Categorias $categorias)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Inertia::render('Categorias/Form', [
            'categoria' => Categorias::find($id)->load("restaurante")
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function update(CategoriasRequest $request, $id)
    {
        // dd($request->all());
        $form = json_decode($request->form, true);
        $categoria = Categorias::find($id);

        if ($request->hasFile('imagen')) {
            // Se valida si existe una url en el campo imagen
            if ($categoria->imagen) {
                // Se valida si la imagen existe
                $imagen_name = explode("/", $categoria->imagen);
                $exists = Storage::disk('public')->exists("categorias/" . $imagen_name[2]);

                // Se elimina la imagen anterior
                if ($exists) {
                    Storage::disk('public')->delete("categorias/" . $imagen_name[2]);
                }
            }

            // Se guarda la imagen y se obtiene la url
            $url_imagen = Storage::putFile('public/categorias', $request['imagen']);
            $form['imagen'] = $url_imagen;
        }

        $categoria->fill($form)->save();
        return redirect("categorias");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Categorias  $categorias
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categoria = Categorias::find($id);

        // Se valida si existe una url en el campo imagen
        if ($categoria->imagen) {
            // Se valida si la imagen existe
            $imagen_name = explode("/", $categoria->imagen);
            $exists = Storage::disk('public')->exists("categorias/" . $imagen_name[2]);

            // Se elimina la imagen anterior
            if ($exists) {
                Storage::disk('public')->delete("categorias/" . $imagen_name[2]);
            }
        }

        // Se valida si el registro, se encuentra en uso por la tabla productos
        $exists = $categoria->productos()->exists();

        if ($exists) {
            return back()->withErrors(["is_related" => true]);
        } else {
            $categoria->delete();
        }

        return back();
    }

    public function cambiarEstado($id)
    {
        $categoria = Categorias::find($id);
        $categoria->activo = $categoria->activo ? 0 : 1;
        $categoria->save();

        return back();
    }
}
