<?php

namespace App\Http\Controllers;

use App\Models\ZonasEntregas;
use App\Models\Restaurantes;
use App\Models\CoordenadasZonasEntregas;
use Inertia\Inertia;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Requests\ZonasEntregasRequest;

class ZonasEntregasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restaurantes = Restaurantes::orderBy('nombre')->get();
        $data = $restaurantes->map(function ($restaurante) {
            return [
                'id' => $restaurante->id,
                'nombre' => $restaurante->nombre,
                'descripcion' => $restaurante->descripcion,
                'activo' => $restaurante->activo,
                'zonasEntregas' => $restaurante->zonasEntregas,
            ];
        });

        return Inertia::render('Zonas_Entrega/Index', ['zonasEntregas' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    public function formulario($id)
    {

        $restaurante = Restaurantes::find($id, ['id', 'nombre']);

        return Inertia::render('Zonas_Entrega/Form', ['zonasEntregasCreate' => $restaurante]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ZonasEntregasRequest $request)
    {
        $zona_entrega = ZonasEntregas::create($request->form);

        foreach ($request->coors as $coor) {
            CoordenadasZonasEntregas::create([
                'latitud' => $coor[0],
                'longitud' => $coor[1],
                'altura' => 0,
                'id_zona_entrega' => $zona_entrega->id
            ]);
        }

        return redirect("zonasentregas");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ZonasEntregas  $zonasEntregas
     * @return \Illuminate\Http\Response
     */
    public function show(ZonasEntregas $zonasEntregas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ZonasEntregas  $zonasEntregas
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        return Inertia::render('Zonas_Entrega/Form', [
            'zonasEntregasEdit' => ZonasEntregas::find($id)->load("restaurantes", "coordenadas")
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ZonasEntregas  $zonasEntregas
     * @return \Illuminate\Http\Response
     */
    public function update(ZonasEntregasRequest $request, $id)
    {
        ZonasEntregas::find($id)->fill($request->form)->save();

        CoordenadasZonasEntregas::where('id_zona_entrega', $id)->delete();

        foreach ($request->coors as $coor) {
            CoordenadasZonasEntregas::create([
                'latitud' => $coor[0],
                'longitud' => $coor[1],
                'altura' => 0,
                'id_zona_entrega' => $id
            ]);
        }

        return Redirect::route('zonasentregas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ZonasEntregas  $zonasEntregas
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // ZonasEntregas::where('id', $id)->delete();
        // return Redirect::back()->with('success', 'Registro Eliminado');
        $ZonasEntrega = ZonasEntregas::find($id);

        // Se valida si el registro, se encuentra en uso por la tabla productos
        $coordenadas = $ZonasEntrega->coordenadas()->exists();
        $restaurantes = $ZonasEntrega->restaurantes()->exists();

        if ($coordenadas || $restaurantes) {
            return back()->withErrors(["is_related" => true]);
        } else {

            $ZonasEntrega->delete();
        }

        return back();
    }

    public function zonasEntregaActivo($id)
    {
        $zona = ZonasEntregas::find($id);
        $zona->activo = $zona->activo == 0 ? 1 : 0;
        $zona->save();
        return back();
    }
}
