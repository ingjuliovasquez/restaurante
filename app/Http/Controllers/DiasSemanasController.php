<?php

namespace App\Http\Controllers;

use App\Models\DiasSemanas;
use Illuminate\Http\Request;

class DiasSemanasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DiasSemanas  $diasSemanas
     * @return \Illuminate\Http\Response
     */
    public function show(DiasSemanas $diasSemanas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DiasSemanas  $diasSemanas
     * @return \Illuminate\Http\Response
     */
    public function edit(DiasSemanas $diasSemanas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DiasSemanas  $diasSemanas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DiasSemanas $diasSemanas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DiasSemanas  $diasSemanas
     * @return \Illuminate\Http\Response
     */
    public function destroy(DiasSemanas $diasSemanas)
    {
        //
    }
}
