<?php

namespace App\Http\Controllers;

use App\Models\Categorias;
use App\Models\Productos;
use App\Models\Restaurantes;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\ProductosRequest;


class ProductosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $restaurantes = Restaurantes::with('productos.categoria')->orderBy('nombre')->get();
        $data = $restaurantes->map(function ($restaurante) {
            return [
                'id' => $restaurante->id,
                'nombre' => $restaurante->nombre,
                'productos' => $restaurante->productos,
            ];
        });

        return Inertia::render('Productos/Index', ['Restaurantes' => $data ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    
    public function formulario($id)
    {
        $categorias = Categorias::orderBy('nombre')->where('id_restaurante',$id)->get();
        $data = $categorias->map(function ($categoria) {
            return [
                'id' => $categoria->id,
                'nombre' => $categoria->nombre,
            ];
        });
        $restaurante = Restaurantes::find($id, ['id', 'nombre']);
        return Inertia::render('Productos/Form', ['productosCreate' => $restaurante,'categorias' => $data]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductosRequest $request)
    {
        $form = json_decode($request->form, true);

        if ($request->hasFile('imagen')) {
            $url_imagen = Storage::putFile('public/productos', $request['imagen']);
            $form['imagen'] = $url_imagen;
        }

        Productos::create($form);
        return redirect("productos");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function show(Productos $productos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $productos = Productos::with('categoria')->find($id);
        $categorias = Categorias::orderBy('nombre')->where('id_restaurante',$productos->id_restaurante)->get();
        $data = $categorias->map(function ($categoria) {
            return [
                'id' => $categoria->id,
                'nombre' => $categoria->nombre,
            ];
        });
        return Inertia::render('Productos/Form', ['productoEdit' => $productos,'categorias' => $data]);

    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function update(ProductosRequest $request,$id)
    {
     
        $form = json_decode($request->form, true);
        $producto = Productos::find($id);

        if ($request->hasFile('imagen')) {
            // Se valida si existe una url en el campo imagen
            if ($producto->imagen) {
                // Se valida si la imagen existe
                $imagen_name = explode("/", $producto->imagen);
                $exists = Storage::disk('public')->exists("productos/" . $imagen_name[2]);

                // Se elimina la imagen anterior
                if ($exists) {
                    Storage::disk('public')->delete("productos/" . $imagen_name[2]);
                }
            }

            // Se guarda la imagen y se obtiene la url
            $url_imagen = Storage::putFile('public/productos', $request['imagen']);
            $form['imagen'] = $url_imagen;
        }

        $producto->fill($form)->save();
        return redirect("productos");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Productos  $productos
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto = Productos::find($id);

        // Se valida si existe una url en el campo imagen
        if ($producto->imagen) {
            // Se valida si la imagen existe
            $imagen_name = explode("/", $producto->imagen);
            $exists = Storage::disk('public')->exists("productos/" . $imagen_name[2]);

            // Se elimina la imagen anterior
            if ($exists) {
                Storage::disk('public')->delete("productos/" . $imagen_name[2]);
            }
        }

        // Se valida si el registro, se encuentra en uso por la tabla productos
        $exists = $producto->categoria()->exists();
        $exists2 = $producto->detallePedido()->exists();
        

        if ($exists || $exists2) {
            return back()->withErrors(["is_related" => true]);
        } else {
            $producto->delete();
        }

        return back();
    }
    public function productosActivo($id)
    {
        $zona = Productos::find($id);
        $zona->activo = $zona->activo == 0 ? 1 : 0;
        $zona->save();
        return back();
    }
}
