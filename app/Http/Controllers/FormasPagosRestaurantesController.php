<?php

namespace App\Http\Controllers;

use App\Models\FormasPagosRestaurantes;
use Illuminate\Http\Request;

class FormasPagosRestaurantesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FormasPagosRestaurantes  $formasPagosRestaurantes
     * @return \Illuminate\Http\Response
     */
    public function show(FormasPagosRestaurantes $formasPagosRestaurantes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FormasPagosRestaurantes  $formasPagosRestaurantes
     * @return \Illuminate\Http\Response
     */
    public function edit(FormasPagosRestaurantes $formasPagosRestaurantes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FormasPagosRestaurantes  $formasPagosRestaurantes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FormasPagosRestaurantes $formasPagosRestaurantes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FormasPagosRestaurantes  $formasPagosRestaurantes
     * @return \Illuminate\Http\Response
     */
    public function destroy(FormasPagosRestaurantes $formasPagosRestaurantes)
    {
        //
    }
}
