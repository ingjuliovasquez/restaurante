<?php

namespace App\Http\Controllers;

use App\Models\DetallesPedidos;
use Illuminate\Http\Request;

class DetallesPedidosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DetallesPedidos  $detallesPedidos
     * @return \Illuminate\Http\Response
     */
    public function show(DetallesPedidos $detallesPedidos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DetallesPedidos  $detallesPedidos
     * @return \Illuminate\Http\Response
     */
    public function edit(DetallesPedidos $detallesPedidos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DetallesPedidos  $detallesPedidos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DetallesPedidos $detallesPedidos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DetallesPedidos  $detallesPedidos
     * @return \Illuminate\Http\Response
     */
    public function destroy(DetallesPedidos $detallesPedidos)
    {
        //
    }
}
