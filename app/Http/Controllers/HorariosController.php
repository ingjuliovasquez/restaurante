<?php

namespace App\Http\Controllers;

use App\Models\Horarios;
use App\Models\DiasSemanas;
use App\Models\Restaurantes;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Validator;

class HorariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Inertia::render('Horarios/Index', [
            'dias' => DiasSemanas::all()->map(function ($dia) {
                return [
                    'id_dia_semana' => $dia->id,
                    'id_restaurante' => "",
                    'dia' => $dia->nombre,
                    'hora_i' => "00:00",
                    'hora_f' => "00:00",
                    'check' => false,
                ];
            }),
            'restaurantes' => Restaurantes::all()->map(function ($restaurante) {
                return [
                    'id' => $restaurante->id,
                    'nombre' => $restaurante->nombre,
                    'horarios' => $restaurante->horarios
                ];
            })
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        foreach ($request->all() as $horario) {
            if ($horario['check']) {

                Validator::make($horario, [
                    'hora_i' => 'required',
                    'hora_f' => 'required',
                ])
                ->validate();

               $horaInicio = date('H:i',strtotime($horario['hora_i']));
               $horaFin = date('H:i',strtotime($horario['hora_f']));

                if($horaInicio >= $horaFin){
                return Inertia::render('Horarios/Index', ['answers' => '1']);
                } else {
                Horarios::updateOrCreate(
                    [
                        'id_dia_semana' => $horario['id_dia_semana'],
                        'id_restaurante' => $horario['id_restaurante'],
                    ],
                    [
                        'hora_inicio' => $horario['hora_i'],
                        'hora_fin' => $horario['hora_f'],
                    ]
                );
            }

            } else {
                Horarios::where([
                    ['id_dia_semana', $horario['id_dia_semana']],
                    ['id_restaurante', $horario['id_restaurante']]
                ])
                ->delete();
            }
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Horarios  $horarios
     * @return \Illuminate\Http\Response
     */
    public function show(Horarios $horarios)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Horarios  $horarios
     * @return \Illuminate\Http\Response
     */
    public function edit(Horarios $horarios)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Horarios  $horarios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Horarios $horarios)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Horarios  $horarios
     * @return \Illuminate\Http\Response
     */
    public function destroy(Horarios $horarios)
    {
        //
    }
}
