<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Restaurantes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use App\Http\Requests\UsuariosRequest;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::where('id_rol', 3)->orderBy('name')->paginate(6);
        $data = $usuarios->map(function ($user) {
            return [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
                'activo' => $user->activo
            ];
        });

        $restaurantes = Restaurantes::all()->map(function ($restaurante) {
            return [
                'id' => $restaurante->id,
                'nombre' => $restaurante->nombre,
                'usuarios' => $restaurante->usuarios
            ];
        });

        return Inertia::render('Usuarios/Index', [
            'Usuarios' => $data,
            'restaurantes' => $restaurantes
        ]);
    }

    public function formulario($id)
    {
        $restaurante = Restaurantes::find($id, ['id', 'nombre']);
        return Inertia::render('Usuarios/Form', ['restaurante' => $restaurante]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $restaurantes = Restaurantes::all()->map(function ($restaurante) {
            return [
                'id' => $restaurante->id,
                'nombre' => $restaurante->nombre,
                'horarios' => $restaurante->horarios
            ];
        });
        return Inertia::render('Usuarios/Form', ['restaurantes' => $restaurantes, 'usuario' => [
            'email' => '',
            'name' => '',
            'password' => ''
        ]]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsuariosRequest $request)
    {

        $form = json_decode($request->form, true);

        $password = Hash::make($form['password']);
        User::updateOrCreate(
            [
            'email' => $form['email'],
            ],
            [
                'name' => $form['name'],
                'password' => $password,
                'id_rol' => 3,
                'id_restaurante' => $form['id_restaurante'],
                'activo' => true,
                'editable' => true
            ]
        );

        return Redirect::route('usuarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Inertia::render('Usuarios/Form', [
            'usuario' => User::find($id)->load("restaurante")
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsuariosRequest $request, $id)
    {

        $form = json_decode($request->form, true);
        $password = Hash::make($form['password']);
        $form['password'] = $password;
        $usuario = User::find($id);
        $usuario->fill($form)->save();
        return redirect("usuarios");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function usuarioActivo($id)
    {
        $user = User::find($id);
        $user->activo = $user->activo == 0 ? 1 : 0;
        $user->save();

        return back();
    }
}