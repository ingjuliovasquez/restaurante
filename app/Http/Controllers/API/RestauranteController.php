<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Categorias;
use App\Models\Restaurantes;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RestauranteController extends Controller
{
    /**
     * Retorna los datos del restaurante con el id seleccionado
     * @param Request $request
     */
    public function getRestauranteData(Request $request)
    {
        return response(Restaurantes::where('id', $request->id)->first());
    }

    /**
     * Retorna si el restaurante está abierto actualmente
     */
    public function getIsRestaurantOpen(Request $request)
    {
        try {
            if ($this->isRestaurantOpen($request->id)) {
                return response(["isOpen" => true]);
            } else {
                return response(["isOpen" => false]);
            }
        } catch (\Exception $e) {
            return response($e, 500);
        }
    }

    /**
     * Verifica si el restaurante está abierto.
     */
    public function isRestaurantOpen($idRestaurant)
    {
        $restauranteId = $idRestaurant;
        $diaDeHoy = ucwords(Carbon::today("America/Mexico_City")->locale("es_MX")->getTranslatedDayName());

        $codigoDia = DB::table("dias_semanas")->where("nombre", $diaDeHoy)->get()->first();

        $rangoHorario = DB::table("horarios")->where("id_restaurante", $restauranteId)->where("id_dia_semana", $codigoDia->id)->get();

        $rangoHorarioNuevo = $rangoHorario->first();

        $horaActual = date("H:i:s");
        $horaFin = $rangoHorarioNuevo->hora_fin;
        $horaInicio = $rangoHorarioNuevo->hora_inicio;

        if ($horaActual >= $horaInicio && $horaActual < $horaFin) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Obtener todas las categorias del restaurante
     */
    public function getAllCategories(Request $request)
    {
        return response(Categorias::where("id_restaurante", $request->id)->get());
    }
}
