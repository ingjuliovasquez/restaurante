<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        // Validando los datos del usuario.
        $request->validate([
            "email" => ["required", "email"],
            "password" => ["required"],
            "device_name" => ["required"],
        ]);

        if (
            Auth::attempt([
                "email" => $request->email,
                "password" => $request->password,
            ])
        ) {
            // Generando y retornando token.
            $user = Auth::user();
            $token = $user->createToken($request->device_name)->plainTextToken;

            return response()->json([
                "token" => $token,
                "user" => $user,
            ]);
        } else {
            throw ValidationException::withMessages([
                "email" => ["The provided credentials are incorrect."],
            ]);
        }
    }
}
