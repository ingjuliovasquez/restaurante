<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Pedidos;
use App\Models\Productos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PedidosController extends Controller
{
    public function getPedidosRestaurante(Request $request)
    {
        $pedidos = Pedidos::where('id_restaurante', $request->id)->where("fecha_hora_enviado", 0)->get();

        return response($pedidos);
    }

    public function getPedido(Request $request)
    {
        $pedido = Pedidos::find($request->id);
        $productosIds = DB::table('detalles_pedidos')->where("id_pedido", $pedido->id)->get();
        $productosData = collect();

        $productosIds->each(function ($value) use ($productosData) {
            $productosData->push(
                DB::table("productos")->where("id", $value->id)->first()
            );

        });

        return response(
            [
                "pedido" => $pedido,
                "productos" => $productosData,
                "pivote" => $productosIds,
            ]
        );
    }
}
