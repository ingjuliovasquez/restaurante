<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Productos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AgotadosController extends Controller
{

    public function getAllProducts(Request $request)
    {
        $products = collect();

        if (isset($request->category) && $request->category != 0) {
            $products = DB::table("productos")->where("id_categoria", $request->category)->get();
        } else {
            $products = DB::table("productos")->where("id_restaurante", $request->id)->get();
        }

        return response($products);
    }

    public function saveProductsChanges(Request $request)
    {
        $products = collect($request->products);

        try {
            $products->each(function ($product) {
                DB::table("productos")
                    ->where("id", $product["productId"])
                    ->update([
                        "agotado" => $product["agotado"]
                    ]);
            });

            $productos = DB::table("productos")->get();
            return response(compact("productos"));
        } catch (\Throwable $e) {
            return response($e, 500);
        }
    }

    public function saveOrdersChanges(Request $request)
    {
        $pedido = DB::table("pedidos")->where("id", $request->id)->first();

        if (isset($pedido->tiempo_estimado_entrega)) {
            $pedido->update([
                "tiempo_estimado_entrega" => $request->tiempo_estimado_entrega,
                "comentario" => $request->comentario
            ]);
        } else {
            $pedido->update(["tiempo_estimado_entrega" => $request->tiempo_estimado_entrega]);
        }

        return response($pedido);
    }

    public function getAllCategories(Request $request)
    {
        return response(DB::table("categorias")->where("id_restaurante", $request->id)->get());
    }
}
