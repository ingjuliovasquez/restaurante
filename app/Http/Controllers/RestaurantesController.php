<?php

namespace App\Http\Controllers;

use App\Http\Requests\RestaurantesRequest;
use App\Models\Restaurantes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Inertia\Inertia;

class RestaurantesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $restaurantes = Restaurantes::orderBy('nombre')->get();
        $data = $restaurantes->map(function ($restaurante) {
            return [
                'id' => $restaurante->id,
                'nombre' => $restaurante->nombre,
                'descripcion' => $restaurante->descripcion,
                'activo' => $restaurante->activo,
            ];
        });

        return Inertia::render('Restaurantes/Index', ['restaurantes' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Inertia::render('Restaurantes/Form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RestaurantesRequest $request)
    {
        $form = json_decode($request->form, true);

        if ($request->hasFile('imagen_logo') && $request->hasFile('imagen_fondo_aplicacion_movil')) {
            $url_imagen = Storage::disk('public')->putFile('restaurantes', $request['imagen_logo']);
            $form['imagen_logo'] = $url_imagen;

            $url_imagen2 = Storage::disk('public')->putFile('estaurantes', $request['imagen_fondo_aplicacion_movil']);
            $form['imagen_fondo_aplicacion_movil'] = $url_imagen2;
        }

        Restaurantes::create($form);
        return redirect("restaurantes");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Restaurantes  $restaurantes
     * @return \Illuminate\Http\Response
     */
    public function show(Restaurantes $restaurantes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Restaurantes  $restaurantes
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $restaurante = Restaurantes::find($id);
        return Inertia::render('Restaurantes/Form', [
            'restaurante' => $restaurante,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Restaurantes  $restaurantes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $form = json_decode($request->form, true);
      
        if ($request['imagen_logo'] && $request['imagen_fondo_aplicacion_movil'] == null) {
            // Se validan los campos del form
            Validator::make(
                $form,
                [
                    'nombre' => [
                        'required',
                        'max:50',
                        Rule::unique('restaurantes')->ignore($id),
                    ],
                    'descripcion' => 'required|max:150',
                    'telefono' => 'required|max:15',
                    'contacto' => 'required|max:30',
                    'correo_electronico' => 'required|regex:/(.+)@(.+)\.(.+)/i',
                    'direccion' => 'required|max:50',
                    'rfc' => 'max:13',
                    'imagen_logo' => 'mimes:png,jpg,jpeg',

                ],
                [

                    'nombre.required' => 'Por favor ingrese un nombre de restaurante',
                    'nombre.unique' => 'Este nombre de restaurante ya esta en uso',
                    'descripcion.required' => 'Por favor ingrese, la descripción del restaurante',
                    'descripcion.max' => 'Por favor, ingrese un valor, entre 1 y 50 digitos',
                    'telefono.required' => 'Por favor ingrese, el teléfono del restaurante',
                    'telefono.max' => 'Por favor, ingrese un valor, entre 1 y 15 digitos',
                    'contacto.required' => 'Por favor ingrese, el contacto del restaurante',
                    'contacto.max' => 'Por favor, ingrese un valor, entre 1 y 30 digitos',
                    'correo_electronico.required' => 'Por favor ingrese, el correo del restaurante',
                    'correo_electronico.email' => 'Por favor, ingrese un correo valido',
                    'direccion.required' => 'Por favor ingrese, la dirección del restaurante',
                    'direccion.max' => 'Por favor, ingrese un valor, entre 1 y 150 digitos',
                    'rfc.max' => 'Por favor, ingrese un valor, entre 1 y 13 digitos',
                    'imagen_logo.mimes' => 'La imagen debe tener formato png, jpg o jpeg',

                ]
            )->validate();
            Validator::make(
                request()->all(),
                [

                    'imagen_logo' => 'mimes:png,jpg,jpeg',

                ],
                [

                    'imagen_logo.mimes' => 'La imagen debe tener formato png, jpg o jpeg',

                ]
            )->validate();
            $form = json_decode($request->form, true);
            $restaurante = Restaurantes::find($id);

            // Se valida si la imagen existe
            $imagen_name = explode("/", $restaurante->imagen_logo);
            $exists = Storage::disk('public')->exists("restaurantes/" . $imagen_name[2]);

            // Se elimina la imagen anterior
            if ($exists) {
                Storage::disk('public')->delete("restaurantes/" . $imagen_name[2]);
            }

            // Se guarda la imagen y se obtiene la url
            $url_imagen = Storage::disk('public')->putFile('public/restaurantes', $request['imagen_logo']);
            $form['imagen_logo'] = $url_imagen;

            $restaurante->fill($form)->save();
            return Redirect::route('restaurantes.index');
        } elseif ($request['imagen_fondo_aplicacion_movil'] && $request['imagen_logo'] == null) {
            // Se validan los campos del form
            Validator::make(
                $form,
                [

                    'nombre' => [
                        'required',
                        'max:50',
                        Rule::unique('restaurantes')->ignore($id),
                    ],
                    'descripcion' => 'required|max:150',
                    'telefono' => 'required|max:15',
                    'contacto' => 'required|max:30',
                    'correo_electronico' => 'required|max:50',
                    'direccion' => 'required|max:50',
                    'rfc' => 'max:13',
                    'imagen_fondo_aplicacion_movil' => 'mimes:png,jpg,jpeg',
                ],
                [

                    'nombre.required' => 'Por favor ingrese un nombre de restaurante',
                    'nombre.unique' => 'Este nombre de restaurante ya esta en uso',
                    'descripcion.required' => 'Por favor ingrese, la descripción del restaurante',
                    'descripcion.max' => 'Por favor, ingrese un valor, entre 1 y 50 digitos',
                    'telefono.required' => 'Por favor ingrese, el teléfono del restaurante',
                    'telefono.max' => 'Por favor, ingrese un valor, entre 1 y 15 digitos',
                    'contacto.required' => 'Por favor ingrese, el contacto del restaurante',
                    'contacto.max' => 'Por favor, ingrese un valor, entre 1 y 30 digitos',
                    'correo_electronico.required' => 'Por favor ingrese, el correo del restaurante',
                    'correo_electronico.max' => 'Por favor, ingrese un valor, entre 1 y 50 digitos',
                    'direccion.required' => 'Por favor ingrese, la dirección del restaurante',
                    'direccion.max' => 'Por favor, ingrese un valor, entre 1 y 150 digitos',
                    'rfc.max' => 'Por favor, ingrese un valor, entre 1 y 13 digitos',
                    'imagen_fondo_aplicacion_movil.mimes' => 'La imagen debe tener formato png, jpg o jpeg',
                ]
            )->validate();
            Validator::make(
                request()->all(),
                [

                    'imagen_fondo_aplicacion_movil' => 'mimes:png,jpg,jpeg',

                ],
                [

                    'imagen_fondo_aplicacion_movil.mimes' => 'La imagen debe tener formato png, jpg o jpeg',

                ]
            )->validate();
            $form = json_decode($request->form, true);
            $restaurante = Restaurantes::find($id);

            // FONDO IMAGEN
            $imagen_name2 = explode("/", $restaurante->imagen_fondo_aplicacion_movil);
            $exists2 = Storage::disk('public')->exists("restaurantes/" . $imagen_name2[2]);

            // Se elimina la imagen anterior
            if ($exists2) {
                Storage::disk('public')->delete("restaurantes/" . $imagen_name2[2]);
            }

            $url_imagen2 = Storage::disk('public')->putFile('public/restaurantes', $request['imagen_fondo_aplicacion_movil']);
            $form['imagen_fondo_aplicacion_movil'] = $url_imagen2;

            $restaurante->fill($form)->save();
            return Redirect::route('restaurantes.index');
        } elseif ($request['imagen_fondo_aplicacion_movil'] && $request['imagen_logo']) {
            Validator::make(
                $form,
                [

                    'nombre' => [
                        'required',
                        'max:50',
                        Rule::unique('restaurantes')->ignore($id),
                    ],
                    'descripcion' => 'required|max:150',
                    'telefono' => 'required|max:15',
                    'contacto' => 'required|max:30',
                    'correo_electronico' => 'required|max:50',
                    'direccion' => 'required|max:50',
                    'rfc' => 'max:13',
                    'imagen_fondo_aplicacion_movil' => 'mimes:png,jpg,jpeg',
                    'imagen_logo' => 'mimes:png,jpg,jpeg',
                ],
                [

                    'nombre.required' => 'Por favor ingrese un nombre de restaurante',
                    'nombre.unique' => 'Este nombre de restaurante ya esta en uso',
                    'descripcion.required' => 'Por favor ingrese, la descripción del restaurante',
                    'descripcion.max' => 'Por favor, ingrese un valor, entre 1 y 50 digitos',
                    'telefono.required' => 'Por favor ingrese, el teléfono del restaurante',
                    'telefono.max' => 'Por favor, ingrese un valor, entre 1 y 15 digitos',
                    'contacto.required' => 'Por favor ingrese, el contacto del restaurante',
                    'contacto.max' => 'Por favor, ingrese un valor, entre 1 y 30 digitos',
                    'correo_electronico.required' => 'Por favor ingrese, el correo del restaurante',
                    'correo_electronico.max' => 'Por favor, ingrese un valor, entre 1 y 50 digitos',
                    'direccion.required' => 'Por favor ingrese, la dirección del restaurante',
                    'direccion.max' => 'Por favor, ingrese un valor, entre 1 y 150 digitos',
                    'rfc.max' => 'Por favor, ingrese un valor, entre 1 y 13 digitos',
                    'imagen_fondo_aplicacion_movil.mimes' => 'La imagen debe tener formato png, jpg o jpeg',
                    'imagen_logo.mimes' => 'La imagen debe tener formato png, jpg o jpeg',

                ]
            )->validate();
            Validator::make(
                request()->all(),
                [

                    'imagen_fondo_aplicacion_movil' => 'mimes:png,jpg,jpeg',
                    'imagen_logo' => 'mimes:png,jpg,jpeg',

                ],
                [

                    'imagen_fondo_aplicacion_movil.mimes' => 'La imagen debe tener formato png, jpg o jpeg',
                    'imagen_logo.mimes' => 'La imagen debe tener formato png, jpg o jpeg',

                ]
            )->validate();
            $form = json_decode($request->form, true);
            $restaurante = Restaurantes::find($id);

            // Se valida si la imagen existe
            $imagen_name = explode("/", $restaurante->imagen_logo);
            $exists = Storage::disk('public')->exists("restaurantes/" . $imagen_name[2]);

            // Se elimina la imagen anterior
            if ($exists) {
                Storage::disk('public')->delete("restaurantes/" . $imagen_name[2]);
            }

            // Se guarda la imagen y se obtiene la url
            $url_imagen = Storage::disk('public')->putFile('public/restaurantes', $request['imagen_logo']);
            $form['imagen_logo'] = $url_imagen;

            // FONDO IMAGEN
            $imagen_name2 = explode("/", $restaurante->imagen_fondo_aplicacion_movil);
            $exists2 = Storage::disk('public')->exists("restaurantes/" . $imagen_name2[2]);

            // Se elimina la imagen anterior
            if ($exists2) {
                Storage::disk('public')->delete("restaurantes/" . $imagen_name2[2]);
            }

            $url_imagen2 = Storage::disk('public')->putFile('public/restaurantes', $request['imagen_fondo_aplicacion_movil']);
            $form['imagen_fondo_aplicacion_movil'] = $url_imagen2;

            $restaurante->fill($form)->save();
            return Redirect::route('restaurantes.index');
        } else {
            $form = json_decode($request->form, true);
            // Se validan los campos del form
            Validator::make(
                $form,
                [
                    'nombre' => [
                        'required',
                        'max:50',
                        Rule::unique('restaurantes')->ignore($id),
                    ],
                    'descripcion' => 'required|max:150',
                    'telefono' => 'required|max:15',
                    'contacto' => 'required|max:30',
                    'correo_electronico' => 'required|max:50',
                    'direccion' => 'required|max:50',
                    'rfc' => 'max:13',
                    'imagen_logo' => 'mimes:png,jpg,jpeg',
                    'imagen_fondo_aplicacion_movil' => 'mimes:png,jpg,jpeg',
                ],
                [
                    'nombre.required' => 'Por favor ingrese un nombre de restaurante',
                    'nombre.unique' => 'Este nombre de restaurante ya esta en uso',
                    'descripcion.required' => 'Por favor ingrese, la descripción del restaurante',
                    'descripcion.max' => 'Por favor, ingrese un valor, entre 1 y 50 digitos',
                    'telefono.required' => 'Por favor ingrese, el teléfono del restaurante',
                    'telefono.max' => 'Por favor, ingrese un valor, entre 1 y 15 digitos',
                    'contacto.required' => 'Por favor ingrese, el contacto del restaurante',
                    'contacto.max' => 'Por favor, ingrese un valor, entre 1 y 30 digitos',
                    'correo_electronico.required' => 'Por favor ingrese, el correo del restaurante',
                    'correo_electronico.max' => 'Por favor, ingrese un valor, entre 1 y 50 digitos',
                    'direccion.required' => 'Por favor ingrese, la dirección del restaurante',
                    'direccion.max' => 'Por favor, ingrese un valor, entre 1 y 150 digitos',
                    'rfc.max' => 'Por favor, ingrese un valor, entre 1 y 13 digitos',
                    'imagen_logo.required' => 'Por favor ingrese, una imagen del restaurante',
                    'imagen_logo.mimes' => 'La imagen debe tener formato png, jpg o jpeg',
                    'imagen_fondo_aplicacion_movil.mimes' => 'La imagen debe tener formato png, jpg o jpeg',
                ]
            )->validate();

            //    dd("holA");
            $restaurante = Restaurantes::find($id);

            // Se valida si viene una imagen para el logo
            if ($request->imagen_logo && $request->imagen_logo != "undefined") {
                // Se valida si la imagen existe
                if ($restaurante->imagen_logo) {
                    $imagen_name = explode("/", $restaurante->imagen_logo);
                    $exists = Storage::disk('public')->exists("restaurantes/" . $imagen_name[2]);
                    if ($exists) {
                        // Se elimina la imagen anterior
                        Storage::disk('public')->delete("restaurantes/" . $imagen_name[2]);
                    }
                    // Se guarda la imagen y se obtiene la url
                    $url_imagen = Storage::disk('public')->putFile('public/restaurantes', $request['imagen_logo']);
                    $form['imagen_logo'] = $url_imagen;
                } else {
                    // Se guarda la imagen y se obtiene la url
                    $url_imagen = Storage::disk('public')->putFile('public/restaurantes', $request['imagen_logo']);
                    $form['imagen_logo'] = $url_imagen;
                }
            }

            // Se valida si viene una imagen para el movil
            if ($request->imagen_fondo_aplicacion_movil && $request->imagen_fondo_aplicacion_movil != "undefined") {
                // FONDO IMAGEN
                if ($restaurante->imagen_fondo_aplicacion_movil) {
                    $imagen_name2 = explode("/", $restaurante->imagen_fondo_aplicacion_movil);
                    $exists2 = Storage::disk('public')->exists("restaurantes/" . $imagen_name2[2]);

                    // Se elimina la imagen anterior
                    if ($exists2) {
                        Storage::disk('public')->delete("restaurantes/" . $imagen_name2[2]);
                    }
                    $url_imagen2 = Storage::disk('public')->putFile('public/restaurantes', $request['imagen_fondo_aplicacion_movil']);
                    $form['imagen_fondo_aplicacion_movil'] = $url_imagen2;
                } else {
                    $url_imagen2 = Storage::disk('public')->putFile('public/restaurantes', $request['imagen_fondo_aplicacion_movil']);
                    $form['imagen_fondo_aplicacion_movil'] = $url_imagen2;
                }
            }

            $restaurante->fill($form)->save();
            return Redirect::route('restaurantes.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Restaurantes  $restaurantes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $restaurante = Restaurantes::find($id);

        // Se valida si el registro, se encuentra en uso por la tabla productos
        $horarios = $restaurante->horarios()->exists();
        $formasPago = $restaurante->formasPago()->exists();
        $categorias = $restaurante->categorias()->exists();
        $pedidos = $restaurante->pedidos()->exists();
        $productos = $restaurante->productos()->exists();
        $users = $restaurante->usuarios()->exists();
        $zonasEntregas = $restaurante->zonasEntregas()->exists();

        if ($horarios || $formasPago || $categorias || $pedidos || $users || $zonasEntregas || $productos) {
            return back()->withErrors(["is_related" => true]);
        } else {
            // Se valida si existe una url en el campo imagen
            if ($restaurante->imagen_logo && $restaurante->imagen_fondo_aplicacion_movil) {
                // Se valida si la imagen existe
                $imagen_name = explode("/", $restaurante->imagen_logo);
                $exists = Storage::disk('public')->exists("restaurantes/" . $imagen_name[2]);

                // FONDO IMAGEN
                $imagen_name2 = explode("/", $restaurante->imagen_fondo_aplicacion_movil);
                $exists2 = Storage::disk('public')->exists("restaurantes/" . $imagen_name2[2]);

                // Se elimina la imagen anterior
                if ($exists && $exists2) {
                    Storage::disk('public')->delete("restaurantes/" . $imagen_name[2]);
                    Storage::disk('public')->delete("restaurantes/" . $imagen_name2[2]);
                }
            }

            $restaurante->delete();
        }

        return back();
    }

    public function restaurantesActivo($id)
    {
        $dashboard = Restaurantes::find($id);
        $dashboard->activo = $dashboard->activo == 0 ? 1 : 0;
        $dashboard->save();
        return back();
    }
}
