<?php

namespace App\Policies;

use App\Models\Roles;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AuthPolicies
{
    use HandlesAuthorization;

    public function isEncargado(User $user)
    {
        try {
            return $user->id_rol == 3;
        } catch (\Throwable $e) {
            return response(["Error" => $e], 500);
        }
    }
}
