<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormasPagos extends Model
{
    use HasFactory;

    public function formasPR() {
        return $this->hasMany("App\Models\FormasPagosRestaurantes", "id_forma_pago");
    }
}
