<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CoordenadasZonasEntregas extends Model
{
    use HasFactory;

    protected $fillable = ['latitud', 'longitud', 'altura', 'id_zona_entrega'];
}
