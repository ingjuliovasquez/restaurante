<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Restaurantes extends Model
{
    use HasFactory;
    public $table = 'restaurantes';
    public $fillable = [
        'id',
        'nombre',
        'descripcion',
        'direccion',
        'telefono',
        'contacto',
        'correo_electronico',
        'rfc',
        'imagen_logo',
        'imagen_fondo_aplicacion_movil',
        'activo',

    ];
    public $attributes = [
        "activo" => 1,
    ];

    public function horarios()
    {
        return $this->hasMany("App\Models\Horarios", "id_restaurante");
    }

    public function formasPago()
    {
        return $this->hasMany('App\Models\FormasPagosRestaurantes', 'id_restaurante');
    }

    public function categorias()
    {
        return $this->hasMany('App\Models\Categorias', 'id_restaurante', 'id')->orderBy('prioridad');
    }

    public function usuarios()
    {
        return $this->hasMany('App\Models\User', 'id_restaurante');
    }

    public function zonasEntregas()
    {
        return $this->hasMany('App\Models\ZonasEntregas', 'id_restaurante', 'id');
    }

    public function productos()
    {
        return $this->hasMany('App\Models\Productos', 'id_restaurante', 'id');
    }
    public function pedidos()
    {
        return $this->hasMany('App\Models\Pedidos', 'id_restaurante', 'id');
    }
    /*  public function formasPago() {
        return $this->belongsToMany('App\Models\FormasPagos', 'App\Models\FormasPagosRestaurantes', 'id_restaurante', 'id_forma_pago');
        return $this->belongsToMany('App\Models\Productos', 'App\Models\Franquicias_Productos', 'Franquicia_id', 'Producto_id')
            ->withPivot('Descuento', 'Activo');
    }*/
}
