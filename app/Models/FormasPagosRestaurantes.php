<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FormasPagosRestaurantes extends Model
{
    use HasFactory;
    protected $fillable = ["id_forma_pago", "activo", "id_restaurante"];

    

}
