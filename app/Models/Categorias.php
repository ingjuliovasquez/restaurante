<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categorias extends Model
{
    use HasFactory;

    protected $fillable = ["nombre", "descripcion", "complemento", "prioridad", "imagen", "activo", "id_restaurante"];
    protected $attributes = ["activo" => 1];

    public function restaurante() {
        return $this->hasOne("App\Models\Restaurantes", "id", "id_restaurante");
    }

    public function productos() {
        return $this->hasMany("App\Models\Productos", "id_categoria");
    }
}
