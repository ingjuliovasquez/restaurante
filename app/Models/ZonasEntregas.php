<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ZonasEntregas extends Model
{
    use HasFactory;
    public $table = 'zonas_entregas';
    public $fillable = [
        'id',
        'nombre',
        'costo_envio',
        'compra_minima',
        'tiempo_traslado',
        'contacto',
        'color',
        'activo',
        'id_restaurante',
    ];
    public $attributes = ['activo' => 1];

    public function coordenadas()
    {
        return $this->hasMany('App\Models\CoordenadasZonasEntregas', 'id_zona_entrega', 'id');
    }

    public function restaurantes(){
        return $this->belongsTo('App\Models\Restaurantes', 'id_restaurante', 'id');
    }

}
