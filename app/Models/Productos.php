<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Productos extends Model
{
    use HasFactory;

    public $table = 'productos';
    public $fillable = [
        'id',
        'nombre',
        'descripcion',
        'preparacion',
        'imagen',
        'agotado',
        'activo',
        'costo',
        'id_categoria',
        'id_restaurante',

    ];
    public $attributes = [
        "activo" => 1,
        "agotado" => 0,
    ];

    public function detallePedido() {
        return $this->hasOne("App\Models\DetallesPedidos", "id_producto","id");
    }

    // public function restaurante(){
    //     return $this->belongsTo('App\Models\Restaurantes', 'id_restaurante', 'id');
    // }

    public function categoria(){
        return $this->belongsTo('App\Models\Categorias', 'id_categoria', 'id');
    }
}
