function clearOptions() {
    // El id del menu  remplasado aqui('div#{IDDELMENU}').
    var options = $('ul#menu-rest').find('a');

    options.each(function() {
        $(this).removeClass('opcions');
    });
}

document.addEventListener('DOMContentLoaded', function(event) {
    // Validamos que exista la variable, esta se elimina al cerrar la pestaña o navegador
    if (!sessionStorage.selectedOption) {
        sessionStorage.selectedOption = 'DefaultValuedashboard';

    }
    // Limpiamos las opciones
    clearOptions();
    // Colocamos la clase para mostrar la opcion seleccionada por ultima vez
    $('#' + sessionStorage.selectedOption).addClass('opcions');

    //addItemForLoadListener();
});


// //SE agrega un item extra de tipo iframe solo para saber cuando el menu se carga este no tiene otro uso 
// function addItemForLoadListener() {
//     var div = document.getElementById('menu-container-main');
//     div.innerHTML += '<iframe id="checkLoad" onload="refresh()" src="" style="display: none;"></iframe>';
// }

//Funcion para guardar la seleccion echa del menu y marcarla
function selectOptionMenu(option) {
    var id = option.id; // Recuperamos el id de la opcion seleccionada
    sessionStorage.selectedOption = id; // Guardamos la opcion
    // Limpiamos las opciones
    clearOptions();
    // Colocamos la clase para mostrar la opcion seleccionada
    $('#' + sessionStorage.selectedOption).addClass('opcions');

}