<x-guest-layout>
<div class="form-container">
    <img src="/img/logo-login.png" class="img" alt="logo" width="300" />

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div>
                <x-jet-label for="name" value="{{ __('Name') }}" />
                <x-jet-input id="name" class="input-login block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
            </div>

            <div class="mt-2">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="input-login block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>

            <div class="mt-2">
                <x-jet-label for="password" value="{{ __('Password') }}" />
                <x-jet-input id="password" class="input-login block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
            </div>

            <div class="mt-2">
                <x-jet-label for="password_confirmation" value="{{ __('Confirm Password') }}" />
                <x-jet-input id="password_confirmation" class="input-login block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
            </div>

           
                 <x-jet-button class="text-center btn-block btn-login btn btn-light mt-4">
                    {{ __('Register') }}
                </x-jet-button>
                
                <div class="text-center ">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>
            </div>
        </form>
    </div>
</x-guest-layout>
<style>
/*Estilos Login*/
.form-container{
    color: black;
    padding: 20px;
    border-radius: 5px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    -moz-transform: translate(-50%, -50%);
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
}
.btn-login{
background-color: #F97A74;
    color: white;
}
.tittles{
color:#bebfc0;
}
.newcta{
color:#8b8c91;
}
.input-login{
background-color:#ebecf0!important;
border:none!important;
}
.btn-login:hover{
background-color:#e86e69;
color:white;
}

/*finanliza Estilos Login*/
</style>