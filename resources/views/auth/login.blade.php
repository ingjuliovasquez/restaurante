<x-guest-layout>
<div class="form-container">
<img src="/img/Logo.png" class="img" alt="logo" width="300"  />
<br><br>

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div>
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full input-login" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Contraseña') }}" />
                <x-jet-input id="password" class="block mt-1 w-full input-login" type="password" name="password" required autocomplete="current-password" />
            </div>

            <x-jet-validation-errors   class="mt-4" />
                <!-- <div class="block mt-4">
                    <label for="remember_me" class="flex items-center">
                        <input id="remember_me" type="checkbox" class="form-checkbox" name="remember">
                        <span class="ml-2 text-sm text-gray-600">{{ __('Recuérdame') }}</span>
                    </label>
                </div> -->

                <x-jet-button class="btn-login btn btn-block btn-light mt-4">
                    {{ __('Login') }}
                </x-jet-button>

                <div class="text-center ">
                    @if (Route::has('password.request'))
                        <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                            {{ __('¿Olvidaste tu contraseña?') }}
                        </a>
                    @endif
                </div>
        </form>
    </div>
</x-guest-layout>

<style>
/*Estilos Login*/
.form-container{
    color: #black;
    padding: 20px;
    border-radius: 5px;

    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    -moz-transform: translate(-50%, -50%);
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
}
.btn-login{
background-color: #F97A74;
    color: white;
}
.tittles{
color:#bebfc0;
}
.newcta{
color:#8b8c91;
}
.input-login{
background-color:#ebecf0!important;
border:none!important;
}
.btn-login:hover{
background-color:#e86e69;
color:white;
}
/*finanliza Estilos Login*/
</style>
