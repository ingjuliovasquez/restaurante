<x-guest-layout>
<div class="form-container">
    <img src="/img/logo-login.png" class="img" alt="logo" width="300" />

        <div class="mb-4 text-sm text-gray-600">
            {{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link .') }}
        </div>

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="block">
                <x-jet-label for="email" value="{{ __('Email') }}" />
                <x-jet-input id="email" class="block mt-1 w-full input-login" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="items-center mt-4">
                <x-jet-button class="text-center btn-block btn-login btn btn-light" >
                    {{ __('Email Password Reset Link') }}
                </x-jet-button>
            </div>
        </form>
    </div>
</x-guest-layout>
<style>
/*Estilos Login*/
.form-container{
    color: black;
    width:400px;
    padding: 20px;
    border-radius: 5px;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    -moz-transform: translate(-50%, -50%);
    -webkit-transform: translate(-50%, -50%);
    -ms-transform: translate(-50%, -50%);
}
.input-login{
background-color:#ebecf0!important;
border:none!important;
}
.btn-login{
background-color: #F97A74;
    color: white;
}
.btn-login:hover{
background-color:#e86e69;
color:white;
}
</style>