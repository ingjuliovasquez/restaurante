<?php

namespace Database\Seeders;

use App\Models\Restaurantes;
use Illuminate\Database\Seeder;

class RestaurantesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Restaurantes::create([
            'nombre' => 'MR DEED’S',
            'descripcion' => 'HOT & FAST - PIZZA, WINGS AND BURGER',
            'direccion' => 'Calle 123',
            'telefono' => '0123456789',
            'contacto' => 'None',
            'correo_electronico' => 'admin@mrdeeds.com',
            'rfc' => '',
            'imagen_logo' => 'public/restaurantes/imagen.png',
            'imagen_fondo_aplicacion_movil' => 'public/restaurantes/imagen2.png',
            'activo' => true
        ]);
    }
}
