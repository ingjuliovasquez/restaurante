<?php

namespace Database\Seeders;

use App\Models\FormasPagosRestaurantes;
use Illuminate\Database\Seeder;

class FormasPagosRestaurantesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FormasPagosRestaurantes::create([
            'activo' => true,
            'id_forma_pago' => 1,
            'id_restaurante' => 1,
        ]);
        FormasPagosRestaurantes::create([
            'activo' => true,
            'id_forma_pago' => 2,
            'id_restaurante' => 1,
        ]);
        FormasPagosRestaurantes::create([
            'activo' => true,
            'id_forma_pago' => 3,
            'id_restaurante' => 1,
        ]);
    }
}
