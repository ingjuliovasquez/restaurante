<?php

namespace Database\Seeders;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        User::create([
            'name' => 'webadmin',
            'email' => 'webadmin@admin.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'editable' => 1,
            'activo' => 1,
            'id_rol' => 1,
            'id_restaurante' => 1,
        ]);

        User::create([
            'name' => 'comensal',
            'email' => 'comensal@admin.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'editable' => 1,
            'activo' => 1,
            'id_rol' => 2,
            'id_restaurante' => 1,
        ]);

        User::create([
            'name' => 'encargado',
            'email' => 'encargado@admin.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'editable' => 1,
            'activo' => 1,
            'id_rol' => 3,
            'id_restaurante' => 1,
        ]);

        User::create([
            'name' => 'repartidor',
            'email' => 'repartidor@admin.com',
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'editable' => 1,
            'activo' => 1,
            'id_rol' => 4,
            'id_restaurante' => 1,
        ]);
    }
}
