<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\DiasSemanas;

class DiasSemanasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DiasSemanas::create([
            'nombre' => 'Domingo',
            'nombre_breve' => 'Do',
            'activo' => 1
        ]);

        DiasSemanas::create([
            'nombre' => 'Lunes',
            'nombre_breve' => 'Lu',
            'activo' => 1
        ]);

        DiasSemanas::create([
            'nombre' => 'Martes',
            'nombre_breve' => 'Mar',
            'activo' => 1
        ]);

        DiasSemanas::create([
            'nombre' => 'Miercoles',
            'nombre_breve' => 'Mi',
            'activo' => 1
        ]);

        DiasSemanas::create([
            'nombre' => 'Jueves',
            'nombre_breve' => 'Ju',
            'activo' => 1
        ]);

        DiasSemanas::create([
            'nombre' => 'Viernes',
            'nombre_breve' => 'Vi',
            'activo' => 1
        ]);

        DiasSemanas::create([
            'nombre' => 'Sabado',
            'nombre_breve' => 'Sa',
            'activo' => 1
        ]);
    }
}
