<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RestaurantesSeeder::class);
        $this->call(CategoriasSeeder::class);
        $this->call(ProductosSeeder::class);
        //\App\Models\Restaurantes::factory(10)->create();
        // \App\Models\DiasSemanas::factory(1)->create();
        $this->call(DiasSemanasSeeder::class);
        /* \App\Models\Horarios::factory(10)->create(); */
        //\App\Models\Categorias::factory(1)->create();

       // \App\Models\Roles::factory(10)->create();
        $this->call(RolesSeeder::class);
        // \App\Models\FormasPagos::factory(10)->create();
        $this->call(FormasPagosSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(FormasPagosRestaurantesSeeder::class);

        \App\Models\ZonasEntregas::factory(10)->create();
        //\App\Models\FormasPagosRestaurantes::factory(10)->create();
        //\App\Models\Pedidos::factory(10)->create();
        //\App\Models\Productos::factory(10)->create();
        //\App\Models\DetallesPedidos::factory(10)->create();
        \App\Models\CoordenadasZonasEntregas::factory(10)->create();
        //\App\Models\FormasPagosRestaurantes::factory(10)->create();
        \App\Models\Comensales::factory(10)->create();
    }
}
