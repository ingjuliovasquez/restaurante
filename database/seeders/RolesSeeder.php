<?php

namespace Database\Seeders;
use App\Models\Roles;

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Roles::create([
            'nombre' => 'webadmin',
            'descripcion' => 'webadmin@admin.com',
            'mostrar_en_flujo_pedido' => 1,
            'mostrar_en_usuarios' => 1,
            'nivel' => 1,
            'activo' => 1,
        ]);
        Roles::create([
            'nombre' => 'comensal',
            'descripcion' => 'comensal@admin.com',
            'mostrar_en_flujo_pedido' => 1,
            'mostrar_en_usuarios' => 1,
            'nivel' => 1,
            'activo' => 1,
        ]);
        Roles::create([
            'nombre' => 'encargado',
            'descripcion' => 'encargado@admin.com',
            'mostrar_en_flujo_pedido' => 1,
            'mostrar_en_usuarios' => 1,
            'nivel' => 1,
            'activo' => 1,
        ]);
        Roles::create([
            'nombre' => 'repartidor',
            'descripcion' => 'repartidor@admin.com',
            'mostrar_en_flujo_pedido' => 1,
            'mostrar_en_usuarios' => 1,
            'nivel' => 1,
            'activo' => 1,
        ]);
    }
}
