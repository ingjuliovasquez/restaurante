<?php

namespace Database\Seeders;

use App\Models\Categorias;
use Illuminate\Database\Seeder;

class CategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categorias::create([
            'nombre' => 'Pizzas',
            'descripcion' => 'Pizzas',
            'complemento' => 1,
            'prioridad' => 1,
            'activo' => 1,
            'id_restaurante' => 1,
        ]);

        Categorias::create([
            'nombre' => 'Pollo',
            'descripcion' => 'Pollo',
            'complemento' => 1,
            'prioridad' => 1,
            'activo' => 1,
            'id_restaurante' => 1,
        ]);

        Categorias::create([
            'nombre' => 'Aderezos',
            'descripcion' => 'Aderezos',
            'complemento' => 1,
            'prioridad' => 1,
            'activo' => 1,
            'id_restaurante' => 1,
        ]);

        Categorias::create([
            'nombre' => 'Entradas',
            'descripcion' => 'Entradas',
            'complemento' => 1,
            'prioridad' => 1,
            'activo' => 1,
            'id_restaurante' => 1,
        ]);

        Categorias::create([
            'nombre' => 'Bebidas',
            'descripcion' => 'bebidas',
            'complemento' => 1,
            'prioridad' => 1,
            'activo' => 1,
            'id_restaurante' => 1,
        ]);

        Categorias::create([
            'nombre' => 'Hamburguesas',
            'descripcion' => 'Hamburguesas',
            'complemento' => 1,
            'prioridad' => 1,
            'activo' => 1,
            'id_restaurante' => 1,
        ]);

        Categorias::create([
            'nombre' => 'Tacos',
            'descripcion' => 'Especialidad de la casa',
            'complemento' => 1,
            'prioridad' => 1,
            'activo' => 1,
            'id_restaurante' => 1,
        ]);
    }
}
