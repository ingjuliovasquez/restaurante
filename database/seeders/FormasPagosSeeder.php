<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\FormasPagos;

class FormasPagosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FormasPagos::create([
            'nombre' => 'Tarjeta de credito',
            'descripcion' => 'Pago con tarjeta de credito',
            'activo' => 1
        ]);

        FormasPagos::create([
            'nombre' => 'Tarjeta de debito',
            'descripcion' => 'Pago con tarjeta de debito',
            'activo' => 1
        ]);

        FormasPagos::create([
            'nombre' => 'Efectivo',
            'descripcion' => 'Pago en efectivo',
            'activo' => 1
        ]);
    }
}
