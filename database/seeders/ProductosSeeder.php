<?php

namespace Database\Seeders;

use App\Models\Productos;
use Illuminate\Database\Seeder;

class ProductosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Productos::create([
            "nombre" => 'Pizza pepperoni',
            "descripcion" => 'Pizza pepperoni',
            "preparacion" => 'Pizza pepperoni',
            "agotado" => 0,
            "activo" => 1,
            "costo" => 90,
            "id_categoria" => 1,
            "id_restaurante" => 1,
        ]);

         Productos::create([
             "nombre" => 'Pizza jamón',
             "descripcion" => 'Pizza jamón',
             "preparacion" => 'Pizza jamón',
             "agotado" => 0,
             "activo" => 1,
             "costo" => 90,
             "id_categoria" => 1,
             "id_restaurante" => 1,
         ]);

         Productos::create([
             "nombre" => 'Pizza tomate con acelgas',
             "descripcion" => 'Pizza tomate',
             "preparacion" => 'Pizza tomate',
             "agotado" => 0,
             "activo" => 1,
             "costo" => 90,
             "id_categoria" => 1,
             "id_restaurante" => 1,
         ]);

         Productos::create([
             "nombre" => 'Alitas',
             "descripcion" => 'Alitas',
             "preparacion" => 'Alitas',
             "agotado" => 0,
             "activo" => 1,
             "costo" => 65,
             "id_categoria" => 2,
             "id_restaurante" => 1,
         ]);

         Productos::create([
             "nombre" => 'Boneless',
             "descripcion" => 'Boneless',
             "preparacion" => 'Boneless',
             "agotado" => 0,
             "activo" => 1,
             "costo" => 65,
             "id_categoria" => 2,
             "id_restaurante" => 1,
         ]);

         Productos::create([
             "nombre" => 'Mayonesa c/chipotle',
             "descripcion" => 'Mayonesa c/chipotle',
             "preparacion" => 'Mayonesa c/chipotle',
             "agotado" => 0,
             "activo" => 1,
             "costo" => 10,
             "id_categoria" => 3,
             "id_restaurante" => 1,
         ]);

         Productos::create([
             "nombre" => 'Crema y jalapeño',
             "descripcion" => 'Crema y jalapeño',
             "preparacion" => 'Crema y jalapeño',
             "agotado" => 0,
             "activo" => 1,
             "costo" => 10,
             "id_categoria" => 3,
             "id_restaurante" => 1,
         ]);

         Productos::create([
             "nombre" => 'Chile y aceite',
             "descripcion" => 'Chile y aceite',
             "preparacion" => 'Chile y aceite',
             "agotado" => 0,
             "activo" => 1,
             "costo" => 10,
             "id_categoria" => 3,
             "id_restaurante" => 1,
         ]);

         Productos::create([
             "nombre" => 'Ranch',
             "descripcion" => 'Ranch',
             "preparacion" => 'Ranch',
             "agotado" => 0,
             "activo" => 1,
             "costo" => 10,
             "id_categoria" => 3,
             "id_restaurante" => 1,
         ]);

         Productos::create([
             "nombre" => 'César',
             "descripcion" => 'César',
             "preparacion" => 'César',
             "agotado" => 0,
             "activo" => 1,
             "costo" => 10,
             "id_categoria" => 3,
             "id_restaurante" => 1,
         ]);

         Productos::create([
             "nombre" => 'Papas',
             "descripcion" => 'Papas',
             "preparacion" => 'Papas',
             "agotado" => 0,
             "activo" => 1,
             "costo" => 40,
             "id_categoria" => 4,
             "id_restaurante" => 1,
         ]);

         Productos::create([
             "nombre" => 'Sticks',
             "descripcion" => 'Palitos dorados, elaborados conparmesano y ajo',
             "preparacion" => 'Sticks',
             "agotado" => 0,
             "activo" => 1,
             "costo" => 40,
             "id_categoria" => 4,
             "id_restaurante" => 1,
         ]);

         Productos::create([
             "nombre" => 'ensalada',
             "descripcion" => 'ensalada',
             "preparacion" => 'ensalada',
             "agotado" => 0,
             "activo" => 1,
             "costo" => 60,
             "id_categoria" => 4,
             "id_restaurante" => 1,
         ]);

         Productos::create([
             "nombre" => 'Refresco',
             "descripcion" => 'Refresco',
             "preparacion" => 'Refresco',
             "agotado" => 0,
             "activo" => 1,
             "costo" => 18,
             "id_categoria" => 5,
             "id_restaurante" => 1,
         ]);

         Productos::create([
             "nombre" => 'Clásica',
             "descripcion" => 'Incluye: Carne 300gr., vegetales,queso fundido, salami, jamón,aderezos',
             "preparacion" => 'Clásica',
             "agotado" => 0,
             "activo" => 1,
             "costo" => 70,
             "id_categoria" => 6,
             "id_restaurante" => 1,
         ]);

         Productos::create([
             "nombre" => 'Taco Louisiana',
             "descripcion" => 'En harina o maíz, Fajita, verduras, pimientos, queso mozzarella, queso cheddar y lechuga',
             "preparacion" => 'taco',
             "agotado" => 0,
             "activo" => 1,
             "costo" => 60,
             "id_categoria" => 7,
             "id_restaurante" => 1,
         ]);
    }
}
