<?php

namespace Database\Factories;

use App\Models\Comensales;
use Illuminate\Database\Eloquent\Factories\Factory;

class ComensalesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Comensales::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //

            'direccion' => $this->faker->unique()->jobTitle,
            'telefono' => 222882374,
            'acepto_politicas_y_condiciones' => 1,
            'activo' => $this->faker->numberBetween($min = 0, $max = 1),
            'id_user' => 1,
        ];
    }
}
