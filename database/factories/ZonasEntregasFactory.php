<?php

namespace Database\Factories;

use App\Models\ZonasEntregas;
use Illuminate\Database\Eloquent\Factories\Factory;

class ZonasEntregasFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ZonasEntregas::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'nombre' => 12,
            'costo_envio' => 1,
            'compra_minima' => 1,
            'tiempo_traslado' => 12.1,
            'color' => "#2c7396",
            'activo' => 1,
            'id_restaurante' => 1,
        ];
    }
}
