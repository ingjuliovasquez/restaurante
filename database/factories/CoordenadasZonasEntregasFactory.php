<?php

namespace Database\Factories;

use App\Models\CoordenadasZonasEntregas;
use Illuminate\Database\Eloquent\Factories\Factory;

class CoordenadasZonasEntregasFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CoordenadasZonasEntregas::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'latitud' => 12.2,
            'longitud' => 12.1,
            'altura' => 1,
            'id_zona_entrega' => 1,

        ];
    }
}
