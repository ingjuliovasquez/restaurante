<?php

namespace Database\Factories;

use App\Models\DetallesPedidos;
use Illuminate\Database\Eloquent\Factories\Factory;

class DetallesPedidosFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DetallesPedidos::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'cantidad' => 12,
            'costo' => 12.1,
            'id_pedido' => 1,
            'id_producto' => 1,
        ];
    }
}
