<?php

namespace Database\Factories;

use App\Models\Roles;
use Illuminate\Database\Eloquent\Factories\Factory;

class RolesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Roles::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->unique()->jobTitle,
            'descripcion' => $this->faker->realText($maxNbChars = 150, $indexSize = 2),
            'mostrar_en_flujo_pedido' => $this->faker->numberBetween($min = 0, $max = 1),
            'mostrar_en_usuarios' => $this->faker->numberBetween($min = 0, $max = 1),
            'nivel' => $this->faker->numberBetween($min = 1, $max = 2),
            'activo' => $this->faker->numberBetween($min = 0, $max = 1),
        ];
    }
}
