<?php

namespace Database\Factories;

use App\Models\FormasPagosRestaurantes;
use Illuminate\Database\Eloquent\Factories\Factory;

class FormasPagosRestaurantesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FormasPagosRestaurantes::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'activo' => $this->faker->numberBetween($min = 0, $max = 1),
            'id_forma_pago' => 1,
            'id_restaurante' => 1,
        ];
    }
}
