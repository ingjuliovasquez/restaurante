<?php

namespace Database\Factories;

use App\Models\Categorias;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoriasFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Categorias::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
  
            'nombre' => 'Grandes',
            'descripcion' => 'xxxxx',
            'complemento' => 1,
            'prioridad' => 1,
            'activo' => 1,
            'id_restaurante' => 1,
     
     

        ];
    }
}
