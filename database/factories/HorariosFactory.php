<?php

namespace Database\Factories;

use App\Models\Horarios;
use Illuminate\Database\Eloquent\Factories\Factory;

class HorariosFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Horarios::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            "hora_inicio" => '22:00',
            "hora_fin" =>  '12:00',
            "id_dia_semana" => 1,
            "id_restaurante" => 1,


        ];
    }
}
