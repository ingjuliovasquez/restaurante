<?php

namespace Database\Factories;

use App\Models\FormasPagos;
use Illuminate\Database\Eloquent\Factories\Factory;

class FormasPagosFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FormasPagos::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'nombre' => 'Deposito',
            'descripcion' => $this->faker->realText($maxNbChars = 150, $indexSize = 2),
            'activo' => $this->faker->numberBetween($min = 0, $max = 1),
        ];
    }
}
