<?php

namespace Database\Factories;

use App\Models\Restaurantes;
use Illuminate\Database\Eloquent\Factories\Factory;

class RestaurantesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Restaurantes::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            'nombre' => $this->faker->unique()->jobTitle,
            'descripcion' => $this->faker->realText($maxNbChars = 150, $indexSize = 2),
            'direccion' => $this->faker->streetAddress,
            'telefono' => $this->faker->e164PhoneNumber,
            'contacto' => $this->faker->realText($maxNbChars = 30, $indexSize = 2),
            'correo_electronico' => $this->faker->unique()->safeEmail,
            'direccion' => $this->faker->realText($maxNbChars = 150, $indexSize = 2),
            'rfc' => $this->faker->realText($maxNbChars = 13, $indexSize = 2),
            'imagen_logo' => $this->faker->imageUrl($width = 640, $height = 480),
            'imagen_fondo_aplicacion_movil' => $this->faker->imageUrl($width = 640, $height = 480),
            'activo' => $this->faker->numberBetween($min = 0, $max = 1),
        ];
    }
}
