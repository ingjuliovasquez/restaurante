<?php

namespace Database\Factories;

use App\Models\DiasSemanas;
use Illuminate\Database\Eloquent\Factories\Factory;

class DiasSemanasFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DiasSemanas::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => 'lunes',
            'nombre_breve' => 'es',
            'activo' => $this->faker->numberBetween($min = 0, $max = 1),
        ];
    }
}
