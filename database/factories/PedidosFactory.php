<?php

namespace Database\Factories;

use App\Models\Pedidos;
use Illuminate\Database\Eloquent\Factories\Factory;

class PedidosFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pedidos::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
            "numero_pedido" => 12,
            "costo_compra" =>  12,
            "direccion" => "sdsddfd", 
            "comentario" => "xcvsdfddf",
            "costo_envio" => 12.2,
            "costo_total" => 12.2,
            "efectivo_pago_exacto" => 1,
            "efectivo_paga_con" => 12.2,
            "tiempo_estimado_entrega" => 1,
           "fecha_hora_solicitado" => 123,
           "fecha_hora_aceptado" => 123,
           "fecha_hora_enviado" => 123,
            'id_comensal' => 1,
            "id_zona_entrega" => 1,
            "id_forma_pago" => 1,
            "id_restaurante" => 1,
        ];
    }
}
