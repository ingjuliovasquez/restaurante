<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categorias', function (Blueprint $table) {
            $table->id();
            $table->string("nombre")->length(50)->unique();
            $table->string("descripcion")->length(150)->nullable();
            $table->boolean("complemento");
            $table->tinyInteger("prioridad");
            $table->string("imagen")->nullable();
            $table->boolean("activo");
            $table->foreignId("id_restaurante");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorias');
    }
}
