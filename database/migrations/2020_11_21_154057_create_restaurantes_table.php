<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurantes', function (Blueprint $table) {
            $table->id();
            $table->string("nombre", 50)->unique();
            $table->string("descripcion", 150);
            $table->string("direccion", 150);
            $table->string("telefono",15);
            $table->string("contacto", 30);
            $table->string("correo_electronico", 50);
            $table->string("rfc", 13)->nullable();
            $table->string("imagen_logo");
            $table->string("imagen_fondo_aplicacion_movil");
            $table->boolean("activo");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurantes');
    }
}
