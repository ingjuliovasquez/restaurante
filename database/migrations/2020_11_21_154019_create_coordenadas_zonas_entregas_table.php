<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoordenadasZonasEntregasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coordenadas_zonas_entregas', function (Blueprint $table) {
            $table->id();
            $table->string("latitud", 50);
            $table->string("longitud", 50);
            $table->decimal("altura",7,2);
            $table->foreignId("id_zona_entrega");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coordenadas_zonas_entregas');
    }
}
