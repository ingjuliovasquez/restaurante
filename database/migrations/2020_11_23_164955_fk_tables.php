<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class FkTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Fk users <-> roles
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('id_rol')->references('id')->on('roles');
        });

        // FK users <-> restaurantes
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('id_restaurante')->references('id')->on('restaurantes');
        });

        // FK categorias <-> restaurantes
        Schema::table('categorias', function (Blueprint $table) {
            $table->foreign('id_restaurante')->references('id')->on('restaurantes');
        });

        // FK productos <-> categorias
        Schema::table('productos', function (Blueprint $table) {
            $table->foreign('id_categoria')->references('id')->on('categorias');
            $table->foreign('id_restaurante')->references('id')->on('restaurantes');
        });

        // FK zonas_entrega <-> restaurantes
        Schema::table('zonas_entregas', function (Blueprint $table) {
            $table->foreign('id_restaurante')->references('id')->on('restaurantes');
        });

        // Fk coordenadas_zonas_entregas <-> zonas_entregas
        Schema::table('coordenadas_zonas_entregas', function (Blueprint $table) {
            $table->foreign('id_zona_entrega')->references('id')->on('zonas_entregas');
        });

        // FK comensales <-> users
        Schema::table('comensales', function (Blueprint $table) {
            $table->foreign('id_user')->references('id')->on('users');
        });

        // FK formas_pagos_restaurantes <-> formas_pagos
        Schema::table('formas_pagos_restaurantes', function (Blueprint $table) {
            $table->foreign('id_forma_pago')->references('id')->on('formas_pagos');
            $table->foreign('id_restaurante')->references('id')->on('restaurantes');
        });


        // FK pedidos <-> comensal
        // FK pedidos <-> zonas_entregas
        // FK pedidos <-> formas_pagos_restaurantes
        // FK pedidos <-> restaurantes
        Schema::table('pedidos', function (Blueprint $table) {
            $table->foreign('id_comensal')->references('id')->on('users');
            $table->foreign('id_zona_entrega')->references('id')->on('zonas_entregas');
            $table->foreign('id_forma_pago')->references('id')->on('formas_pagos_restaurantes');
             $table->foreign('id_restaurante')->references('id')->on('restaurantes');
        });

        // FK detalles_pedidos <-> pedidos
        // FK detalles_pedidos <-> productos
        Schema::table('detalles_pedidos', function (Blueprint $table) {
            $table->foreign('id_pedido')->references('id')->on('pedidos');
            $table->foreign('id_producto')->references('id')->on('productos');
        });

        // FK horarios <-> restaurantes
        // FK horarios <-> dias_semanas
        Schema::table('horarios', function (Blueprint $table) {
            $table->foreign('id_dia_semana')->references('id')->on('dias_semanas');
            $table->foreign('id_restaurante')->references('id')->on('restaurantes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
