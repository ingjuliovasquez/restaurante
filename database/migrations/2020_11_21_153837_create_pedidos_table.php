<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->id();
            $table->bigInteger("numero_pedido")->length(19);
            $table->decimal("costo_compra", 12);
            $table->string("direccion",150);
            $table->string("comentario",255)->nullable();
            $table->decimal("costo_envio",12,2);
            $table->decimal("costo_total",12,2);
            $table->tinyInteger("efectivo_pago_exacto");
            $table->decimal("efectivo_paga_con",12,2);
            $table->tinyInteger("tiempo_estimado_entrega");
            $table->integer("fecha_hora_solicitado");
            $table->integer("fecha_hora_aceptado");
            $table->integer("fecha_hora_enviado");
            $table->foreignId('id_comensal');
            $table->foreignId("id_zona_entrega");
            $table->foreignId("id_forma_pago");
            $table->foreignId("id_restaurante");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedidos');
    }
}
