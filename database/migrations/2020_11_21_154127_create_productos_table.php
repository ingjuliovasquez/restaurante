<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->id();
            $table->string("nombre")->length(50);
            $table->string("descripcion")->length(150)->nullable();
            $table->string("preparacion")->length(255);
            $table->string("imagen", 150)->nullable();
            $table->boolean("agotado");
            $table->decimal("costo",12,2);
            $table->boolean("activo");
            $table->foreignId("id_categoria");
            $table->foreignId("id_restaurante");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
