<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateZonasEntregasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zonas_entregas', function (Blueprint $table) {
            $table->id();
            $table->string("nombre")->length(50);
            $table->decimal("costo_envio",12,2);
            $table->decimal("compra_minima",13,2);
            $table->string("color", 30);
            $table->tinyInteger("tiempo_traslado");
            $table->boolean("activo");
            $table->foreignId("id_restaurante");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('zonas_entregas');
    }
}
