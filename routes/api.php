<?php

use App\Http\Controllers\API\AuthController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\RestauranteController;
use App\Http\Controllers\API\PedidosController;
use App\Http\Controllers\API\AgotadosController;

/**
 * Conjunto de rutas para autenticación de usuarios desde el móvil.
 */
Route::group(["prefix" => "auth"], function () {
    Route::post("/login", [AuthController::class, "login"])->name("APILogin");
});

Route::group(["prefix" => "restaurante"], function () {
    //Obtener la información del restaurante.
    Route::get("/{id}", [RestauranteController::class, "getRestauranteData"]);

    // Obten los pedidos del restaurante.
    Route::get("/pedidos/{id}", [
        PedidosController::class,
        "getPedidosRestaurante",
    ]);

    // Obtener la información de un pedido.
    Route::get("/pedido/{id}", [PedidosController::class, "getPedido"]);

    //Obtener todos los productos de un restaurante.
    Route::get("{id}/productos", [AgotadosController::class, "getAllProducts"]);

    // Guarda los cambios hechos en la sección de pedidos del encargado.
    Route::post("/productos", [AgotadosController::class, "saveProductsChanges"]);

    // Guardar los cambios hechos en la sección de detalles del pedido.
    Route::post("/pedido/{id}", [AgotadosController::class, "saveOrdersChanges"]);

    // Retorna todas las categorías disponibles para el restaurante.
    Route::get("/categorias/{id}", [AgotadosController::class, "getAllCategories"]);
});

Route::group(["prefix" => "comensal"], function () {
    Route::group(["prefix" => "restaurante"], function () {
        //Obtener la información del restaurante.
        Route::get("/{id}", [RestauranteController::class, "getRestauranteData"]);

        // Comprobar si el restaurante está abierto o no.
        Route::get("/isopen/{id}", [RestauranteController::class, "getIsRestaurantOpen"]);

        //Obtener los productos de un restaurante:
        Route::get("{id}/productos", [AgotadosController::class, "getAllProducts"]);

        //Obtener todas las categorias:
        Route::get("{id}/categorias", [RestauranteController::class, "getAllCategories"]);
    });
});
