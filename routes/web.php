<?php

use App\Http\Controllers\CategoriasController;
use App\Http\Controllers\FormasPagosController;
use App\Http\Controllers\HorariosController;
use App\Http\Controllers\ProductosController;
use App\Http\Controllers\RestaurantesController;
use App\Http\Controllers\UsuariosController;
use App\Http\Controllers\ZonasEntregasController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes

1-crear usuao
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('auth.login');
});

//restaurantes route
Route::get('/dashboard', function () {
    return Inertia\Inertia::render('Dashboard');
})->name('dashboard');

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    // Restaurantes Routes
    Route::resource("restaurantes", RestaurantesController::class);
    Route::post("activorestaurante/{id}", [RestaurantesController::class, 'restaurantesActivo'])->name("restaurantes.activo");

    // Horarios Routes
    Route::resource('horarios', HorariosController::class);

    // Zonas Entrega Routes
    Route::resource("zonasentregas", ZonasEntregasController::class);
    Route::get("zonas/{id?}", [ZonasEntregasController::class, "formulario"])->name("formulario_zona");
    Route::get("activozonasentrega/{id}", [ZonasEntregasController::class, 'zonasEntregaActivo'])->name("zonasEntrega.activo");

    // Formas de Pagos Routes
    Route::resource("formas_pagos", FormasPagosController::class);
    Route::get("formas_pago/{id}", [FormasPagosController::class, 'pagosActivo'])->name("formas_pagos.activo");

    // Usuarios Routes
    Route::resource("usuarios", UsuariosController::class);
    Route::get("usuario/{id?}", [UsuariosController::class, "formulario"])->name("formulario_usuario");
    Route::get("estado_usuario/{id?}", [UsuariosController::class, "usuarioActivo"])->name("estado_usuario");

    // Categorias Routes
    Route::resource("categorias", CategoriasController::class);
    Route::get("categoria/{id?}", [CategoriasController::class, "formulario"])->name("formulario_categoria");
    Route::get("estado_categoria/{id?}", [CategoriasController::class, "cambiarEstado"])->name("estado_categoria");

    // Productos Routes      
    Route::resource('productos', ProductosController::class);
    Route::get("producto/{id?}", [ProductosController::class, "formulario"])->name("formulario_producto");
    Route::get("activoproducto/{id}", [ProductosController::class, 'productosActivo'])->name("productos.activo");
});
